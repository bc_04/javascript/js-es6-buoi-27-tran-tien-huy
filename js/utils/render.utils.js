class RenderUtils {
  constructor() {
    this.target = "";
    this.targetData = "";
    this.callBackList = [];
  }

  setTargetEl(targetEl) {
    this.target = document.querySelector(targetEl);
  }

  getTargetEl() {
    return this.target;
  }

  setTargetData(data) {
    this.targetData = data;
  }

  getTargetData() {
    return this.targetData;
  }

  setCallBackList(callBackList) {
    this.callBackList = callBackList;
  }

  getCallBackList() {
    return this.callBackList;
  }

  clearContent() {
    this.getTargetEl().innerHTML = "";
  }

  #createHTMLElement(el, { ...props }) {
    let htmlEl = document.createElement(el);
    Object.assign(htmlEl, props);
    return htmlEl;
  }
  #createGlassInfoHTML() {
    let htmlTemplate = "";
    let target = this.getTargetEl();
    let { name, brand, color, price, description } = this.getTargetData();
    htmlTemplate = `
        <div class="container container-fluid">
            <div class="row">
                <div class="card-info col-12">
                <div class="header d-flex align-items-center mb-3 g-10">
                    <p class="name text-uppercase">${name}</p>
                    <span class="char">-</span>
                    <p class="brand">${brand}</p>
                    <p class="color">(${color})</p>
                </div>
                <div class="body d-flex align-items-center mb-3 g-10">
                    <div
                        class="price bg-danger text-center text-white py-1 px-3 rounded-lg d-flex align-items-center"
                    >
                    <span class="currency">$</span>
                    <span class="number">${price}</span>
                    </div>
                    <p class="stock-status text-success">Stocking</p>
                </div>
                <div class="footer">
                    <p class="desc text-white">${description}</p>
                </div>
                </div>
            </div>
        </div>
    `;
    document.querySelector(".vglasses__info").classList.add("d-block");
    target.innerHTML = htmlTemplate;
  }

  #createGlassListHTML() {
    let htmlTemplate = "";
    let target = this.getTargetEl();
    let data = this.getTargetData();
    let { showGlass } = this.getCallBackList();
    let divContainer = "";
    data.forEach((item, idx) => {
      divContainer = this.#createHTMLElement("div", {
        className: "col-4 col-md-4 col-sm-6 my-3 ",
        id: item.id,
      });
      htmlTemplate = "";
      htmlTemplate = `
        <div class="glass-item" data-id=${item.id}>
            <img src="${item.src}" class="glass-img glass-img-${item.id}" alt="Hinh mat kieng" data-id="${item.id}"/>
        </div>
        `;
      // render html
      divContainer.innerHTML = htmlTemplate;
      target.append(divContainer);
      let glassItem = document.querySelectorAll(`.glass-item`)[idx];
      glassItem.addEventListener("click", showGlass);
    });
  }

  #createGlassHTML() {
    let htmlTemplate = "";
    let target = this.getTargetEl();
    let data = this.getTargetData();
    htmlTemplate = `
        <div class="glass">
            <img src="${data}" class="glass-img" alt="Render hình ảnh mắt kính" />
        </div>
    `;
    target.innerHTML = htmlTemplate;
  }

  bindEvents(_target, event, callback) {
    if (_target instanceof HTMLElement) {
      _target.addEventListener(event, callback);
      return;
    } else {
      document.querySelector(_target).addEventListener(event, callback);
    }
  }

  renderHTML(type) {
    this.clearContent();

    if (type === "glassList") {
      this.#createGlassListHTML();
    }
    if (type === "virtualGlass") {
      this.#createGlassHTML();
    }
    if (type === "glassInfo") {
      this.#createGlassInfoHTML();
    }
  }
}

export { RenderUtils };
