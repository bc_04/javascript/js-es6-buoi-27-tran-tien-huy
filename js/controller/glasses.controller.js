import { RenderUtils } from "../utils/render.utils.js";
class Glasses {
  #renderObject;
  constructor() {
    this.#renderObject = new RenderUtils();
  }
  renderGlassList(target, data, callBacklist) {
    this.#renderObject.setTargetEl(target);
    this.#renderObject.setTargetData(data);
    this.#renderObject.setCallBackList(callBacklist);
    this.#renderObject.renderHTML("glassList");
  }
  renderGlassInfo(target, data) {
    this.#renderObject.setTargetEl(target);
    this.#renderObject.setTargetData(data);
    this.#renderObject.renderHTML("glassInfo");
  }
  renderVirtualGlass(target, data) {
    this.#renderObject.setTargetEl(target);
    this.#renderObject.setTargetData(data);
    this.#renderObject.renderHTML("virtualGlass");
  }
}

export { Glasses };
