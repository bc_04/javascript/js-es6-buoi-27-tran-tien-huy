import { Glasses } from "./controller/glasses.controller.js";
import { dataGlasses } from "./data.js";

let glassControllerObject = new Glasses();
let loadGlassList = () => {
  let callBackList = {
    showGlass,
  };
  glassControllerObject.renderGlassList(
    "#vglassesList",
    dataGlasses,
    callBackList
  );
};

let showGlass = (e) => {
  let gId = e.target.dataset.id;
  let findIdx = dataGlasses.findIndex((glass) => glass.id === gId);
  try {
    if (findIdx < 0) {
      throw `You're trying to search for unreal id ${gId}`;
    }
    console.log(dataGlasses[findIdx]);
    glassControllerObject.renderVirtualGlass(
      ".vglasses__model",
      dataGlasses[findIdx].virtualImg
    );
    glassControllerObject.renderGlassInfo(
      ".vglasses__info",
      dataGlasses[findIdx]
    );

    document.querySelectorAll(".vglasses__card .btn-warning").forEach((btn) => {
      btn.disabled = false;
    });
  } catch (error) {
    console.error(`Error : ${error}`);
  }
};

let removeGlasses = (flag) => {
  let modelVirtualGlass = document.querySelector(".vglasses__model .glass");
  let glassInfo = document.querySelector(".vglasses__info");
  try {
    if (!flag) {
      modelVirtualGlass.classList.add("d-none");
      glassInfo.classList.remove("d-block");
    } else {
      modelVirtualGlass.classList.remove("d-none");
      glassInfo.classList.add("d-block");
    }
  } catch (error) {
    console.error(`Error : ${error.name} - ${error.message}`);
  }
};

loadGlassList();

// bring removeGlasses to global scope
window.removeGlasses = removeGlasses;
